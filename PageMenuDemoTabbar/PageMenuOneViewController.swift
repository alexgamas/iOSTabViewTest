//
//  PageMenuOneViewController.swift
//  PageMenuDemoTabbar
//
//  Created by Niklas Fahl on 1/9/15.
//  Copyright (c) 2015 Niklas Fahl. All rights reserved.
//

import UIKit
import PageMenu

class PageMenuOneViewController: UIViewController, SimpleViewControllerScrollDelegate {
    
    var pageMenu : CAPSPageMenu?
    
    @IBOutlet weak var topView: UIView!
    
    @IBOutlet weak var topo: UIView!
    
    @IBOutlet weak var bottom: UIView!
    
    var inTransition: Bool = false
    
    var controllerArray : [UIViewController] = []
    
    //var beginScrollVertical: CGFloat = 0.0
    
    //var posicaoTopo: CGFloat = 0.0
    
    func beginScroll(id: String, scrollView: UIScrollView) {
        inTransition = true
    }
    
    func endScroll(id: String, scrollView: UIScrollView) {
        inTransition = false
    }
    
    func doScroll(id: String, scrollView: UIScrollView) {
        
        let offset = scrollView.contentOffset

        if id == "noticias_scroll" {
            
            if let menu = pageMenu, inTransition {
                
                if menu.currentPageIndex > 0 && offset.x < 0 {
                    menu.moveToPage(menu.currentPageIndex - 1)
                }
                
                let maxOffset = scrollView.contentSize.width - scrollView.frame.width
                
                if menu.currentPageIndex < controllerArray.count && offset.x > maxOffset {
                    menu.moveToPage(menu.currentPageIndex + 1)
                }
                
                inTransition = false
            }
        }
        
        if id == "main_scroll" {
            
            let topLayoutGuideLenght = topLayoutGuide.length
            
            let constraintAltura = topo.constraints.first { elem in
                return elem.identifier == "altura"
            }
            
            let constraintTopo = topView.constraints.first { elem in
                return elem.identifier == "topo"
            }
            
            if let altura = constraintAltura?.constant {
                let topoY = offset.y
                
                if offset.y < (altura - topLayoutGuideLenght) {
                    constraintTopo?.constant = (-1) * topoY
                }
            }

            
            
            
            
        }
    }
    
    override func viewDidLoad() {
        setupMenu()
    }
    
    func setupMenu() {
        
        for i in 0...3 {
            let controller : SimpleViewController = SimpleViewController(nibName: "SimpleViewController", bundle: nil)
            controller.title = "contr\(i)"
            controller.delegate = self
            controllerArray.append(controller)
        }
        
        let fonte = UIFont(name: "Helvetica Neue", size: 12.0)
        
        let labelColor = UIColor(red: 51.0/255.0, green: 51.0/255.0, blue: 51.0/255.0, alpha: 1.0)
        let indicatorColor = UIColor(red: 0.0/255.0, green: 103.0/255.0, blue: 164.0/255.0, alpha: 1.0)
        
        let parameters: [CAPSPageMenuOption] = [
            .scrollMenuBackgroundColor(UIColor.white),
            .viewBackgroundColor(UIColor.white),
            
            .bottomMenuHairlineColor(indicatorColor),
            .selectionIndicatorColor(indicatorColor),
            .menuHeight(32.0),
            .menuItemFont(fonte!),
            
            .selectedMenuItemLabelColor(labelColor),
            .unselectedMenuItemLabelColor(labelColor),
            .selectionIndicatorHeight(2.0)
        ]
        
        let frame = CGRect(x: 0.0, y: 0.0, width: self.bottom.frame.width, height: self.bottom.frame.height)
        
        pageMenu = CAPSPageMenu(viewControllers: controllerArray, frame: frame, pageMenuOptions: parameters)
        
        self.bottom.addSubview(pageMenu!.view)
    }
    
    @IBAction func topoGestureRecognizer(_ sender: UIPanGestureRecognizer) {
        
        //let translation = sender.translation(in: self.view)
        
        // let inicio = sender.
        
        //let constraintAltura = topo.constraints.first { elem in
        //    return elem.identifier == "altura"
        //}
        
        //let constraintTopo = topView.constraints.first { elem in
        //    return elem.identifier == "topo"
        //}
        
        //let offset = translation.y
        
        //constraintTopo?.constant = (constraintTopo?.constant)! + offset
        //print (offset, constraintTopo?.constant)
        
        
    }
    
}
