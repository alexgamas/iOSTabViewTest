//
//  SimpleViewController.swift
//  PageMenuDemoTabbar
//
//  Created by Alex on 22/08/17.
//  Copyright © 2017 Niklas Fahl. All rights reserved.
//

import UIKit


protocol SimpleViewControllerScrollDelegate: class {
    func beginScroll(id: String, scrollView: UIScrollView)
    func endScroll(id: String, scrollView: UIScrollView)
    func doScroll(id: String, scrollView: UIScrollView)
}


class SimpleViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    weak var delegate: SimpleViewControllerScrollDelegate? = nil
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var scroll: UIScrollView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        scroll.delegate = self
        
        //collectionView.register(SimpleViewCell.self, forCellWithReuseIdentifier: "simpleViewCell")
        
        self.collectionView!.register(UINib(nibName: "SimpleViewCell", bundle: nil), forCellWithReuseIdentifier: "simpleViewCell")
        
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
    }
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        if let _ = delegate {
            let id = getScrollId(forView: scrollView)
            delegate?.beginScroll(id: id, scrollView: scrollView)
        }
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        if let _ = delegate {
            let id = getScrollId(forView: scrollView)
            delegate?.endScroll(id: id, scrollView: scrollView)
        }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if let _ = delegate {
            let id = getScrollId(forView: scrollView)
            delegate?.doScroll(id: id, scrollView: scrollView)
        }
    }
    
    func getScrollId(forView: UIScrollView) -> String{
        
        if forView == scroll {
            return "main_scroll"
        }
        
        if forView == collectionView {
            return "noticias_scroll"
        }
        
        return ""
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 120, height: 21)
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 30
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "simpleViewCell", for: indexPath) as! SimpleViewCell
        
        return cell
    }
    
}
